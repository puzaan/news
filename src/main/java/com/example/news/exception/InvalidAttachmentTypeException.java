package com.example.news.exception;

public class InvalidAttachmentTypeException extends RuntimeException {

    public InvalidAttachmentTypeException(String messege) {
        super(messege);
    }
}
