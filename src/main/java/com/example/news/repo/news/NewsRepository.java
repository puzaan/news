package com.example.news.repo.news;

import com.example.news.entity.news.TheNews;
import com.example.news.enums.NewsType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NewsRepository extends JpaRepository<TheNews, Integer> {

    @Query(value = "SELECT ns FROM TheNews ns where ns.newsType=?1")
    List<TheNews> findAllByNewTypes(NewsType newsType);

    @Query(value = "SELECT ns FROM TheNews  ns WHERE ns.highlight=true")
    List<TheNews> findAllHighlightNewsTrue();

    @Query(value = "SELECT ns from TheNews ns where ns.newsCategory.id=?1")
    List<TheNews> findAllNewByCategory(Integer id);
}

