package com.example.news.repo.attachment;

import com.example.news.entity.attachment.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttachmentRepository extends JpaRepository<Attachment,  Integer> {
}
