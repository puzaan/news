package com.example.news.controller.attachment;

import com.example.news.entity.attachment.Attachment;
import com.example.news.globbal.BaseController;
import com.example.news.service.attachment.AttachmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v1/attachments")
public class AttachmentController extends BaseController {
    private final AttachmentService attachmentService;


    public AttachmentController(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    @PostMapping("/save")
    public ResponseEntity<?> saveFile(@RequestParam("file") MultipartFile file, Attachment attachment) {
        Attachment savedAttachment = attachmentService.saveAttachment(file, attachment);
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.create", customMessageSource.get("attachment")), savedAttachment)
        );
    }

    @GetMapping("/list")
    public ResponseEntity<?> getAllAttachmentDetails() {
        List<Attachment> attachments = attachmentService.getAllAttachmentDetails();
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.get", customMessageSource.get("attachment")), attachments)
        );    }

    @GetMapping("/{id}")
    public ResponseEntity<?> singleAttachmentDetail(@PathVariable("id") Integer attachmentId) {
        Attachment attachment = attachmentService.singleAttachmentDetail(attachmentId);
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.get", customMessageSource.get("attachment")), attachment)
        );

    }

    @GetMapping(value = "/imageView/{id}",
            produces = {MediaType.IMAGE_JPEG_VALUE,MediaType.IMAGE_GIF_VALUE,MediaType.IMAGE_PNG_VALUE}
    )
    public @ResponseBody byte[] getImageWithMediaType(@PathVariable("id") Integer attachmentId) throws IOException {
        return attachmentService.getImageWithmediaType(attachmentId);
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> deleteAttachment(@PathVariable("id") Integer attachmentId){
        attachmentService.deleteAttachment(attachmentId);
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.delete", customMessageSource.get("attachment")), null)
        );
    }
}