package com.example.news.controller.news;


import com.example.news.dto.newsdto.NewsDto;
import com.example.news.entity.attachment.Attachment;
import com.example.news.entity.news.TheNews;
import com.example.news.enums.NewsType;
import com.example.news.globbal.BaseController;
import com.example.news.service.news.NewService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/news")
public class TheNewsController extends BaseController {
    private final NewService newService;

    public TheNewsController(NewService newService) {
        this.newService = newService;
    }

    @PostMapping("/create")
    public ResponseEntity<?> createNews(@RequestBody NewsDto news) {
        TheNews news1 = newService.createNew(news);
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.create", customMessageSource.get("news")),news1)
        );
    }

    @GetMapping()
    public ResponseEntity<?> getAllNews() {
        List<TheNews> newsList = newService.getAllNewList();
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.get.all", customMessageSource.get("news")), newsList)
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> singleNews(@PathVariable("id") Integer id) {
        TheNews news = newService.getNewById(id);
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.get", customMessageSource.get("news")), news)
        );

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteNews(@PathVariable("id") Integer id){
        newService.deleteNew(id);
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.delete", customMessageSource.get("news")), "news Deleted")
        );
    }

    @PutMapping()
    public ResponseEntity<?> UpdateNews(@RequestBody TheNews  news) {
        TheNews news1 = newService.updateNewDetail(news);
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.update", customMessageSource.get("news")), news1)
        );
    }
    @PostMapping("/save-image")
    public ResponseEntity<?> saveNewsImage(@RequestParam("file") MultipartFile file, Attachment attachment,NewsDto newsDto) {
        TheNews news = newService.createNewsWithImage(file,attachment,newsDto);
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.create", customMessageSource.get("news")), news)
        );
    }


    @GetMapping("/new-type/{type}")
    public ResponseEntity<?> getAllNewByNewType(@PathVariable("type") NewsType newsType) {
        List<TheNews> newsList = newService.findAllByNewsType(newsType);
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.get.all", customMessageSource.get("news")), newsList)
        );
    }

    @GetMapping("/highlight-new")
    public ResponseEntity<?> getAllHighlightNews() {
        List<TheNews> newsList = newService.findAllHighlightTrueNews();
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.get.all", customMessageSource.get("news")), newsList)
        );
    }

    @GetMapping("/new-by-category")
    public ResponseEntity<?> getAllNewsBByCategory() {
        Map<String, List<TheNews>> newsByCategoryList = newService.getAllNewsByCategory();
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.get.all", customMessageSource.get("news")), newsByCategoryList)
        );
    }
}
