package com.example.news.controller.newsCategory;

import com.example.news.entity.newsCategory.NewsCategory;
import com.example.news.globbal.BaseController;
import com.example.news.service.newsCategory.NewsCatogoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api/v1/new-cate")
public class NewsCategoryController extends BaseController {
    private final NewsCatogoryService newsCatogoryService;

    public NewsCategoryController(NewsCatogoryService newsCatogoryService) {
        this.newsCatogoryService = newsCatogoryService;
    }

    @PostMapping()
    public ResponseEntity<?> createNewCategory(@RequestBody NewsCategory newsCategory) {
        NewsCategory newsCategory1 = newsCatogoryService.createNewCategory(newsCategory);
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.create", customMessageSource.get("newCategory")),newsCategory1)
        );
    }

    @GetMapping("/list")
    public ResponseEntity<?> getAllNewCategory() {
        List<NewsCategory> newsCategoryList = newsCatogoryService.getAllNewCategoryList();
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.get.all", customMessageSource.get("newCategory")), newsCategoryList)
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> singleNewCategory(@PathVariable("id") Integer id) {
        NewsCategory newsCategory = newsCatogoryService.getNewCategoryById(id);
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.get", customMessageSource.get("newCategory")), newsCategory)
        );

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteNewCategory(@PathVariable("id") Integer id){
        newsCatogoryService.deleteNewCategory(id);
        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.delete", customMessageSource.get("newCategory")), null)
        );
    }

    @PutMapping()
    public ResponseEntity<?> UpdateNewCategory(@RequestBody NewsCategory  newsCategory) {
        NewsCategory newsCategory1 = newsCatogoryService.updateNewCategoryDetail(newsCategory);

        return ResponseEntity.ok(
                successResponse(customMessageSource.get(
                        "crud.update", customMessageSource.get("newCategory")), newsCategory1)
        );
    }
}
