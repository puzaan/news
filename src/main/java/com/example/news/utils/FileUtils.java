package com.example.news.utils;


import com.example.news.exception.InvalidAttachmentTypeException;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

public class FileUtils {

    private final static String rootPath = "attachments";

    public static Path pathFinders(MultipartFile file, String timeContract) throws InvalidAttachmentTypeException {

        if (fileTypeValidation(file)) {

            String path = rootPath.concat("/" + fileHash(file, timeContract)) ;

            Path getPath = Paths.get(path);

            return getPath;
        } else {
            throw new InvalidAttachmentTypeException("Please select image type file for attachment only.");
        }

    }

    public static UUID fileHash(MultipartFile file, String timeContract ) {

        return  UUID.nameUUIDFromBytes(file.getOriginalFilename().replace(file.getOriginalFilename(), timeContract).getBytes());

    }

    private static Boolean fileTypeValidation(MultipartFile file) throws InvalidAttachmentTypeException {

        String[] type = file.getContentType().split("/");

        if (type[1].equalsIgnoreCase("jpg") || type[1].equalsIgnoreCase("png") || type[1].equalsIgnoreCase("jpeg")) {
            return true;
        }
        return false;
    }

}

