package com.example.news.globbal;
import org.springframework.beans.factory.annotation.Autowired;
public class BaseController {

    @Autowired
    protected CustomMessageSource customMessageSource;

    protected String messageCode;

    protected ApiResponse successResponse(String message, Object data) {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setStatus(true);
        apiResponse.setMessage(message);
        apiResponse.setData(data);
        return apiResponse;
    }
    protected ApiResponse errorResponse(String message, Object errors) {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setStatus(false);
        apiResponse.setMessage(message);
        apiResponse.setData(errors);
        return apiResponse;
    }

}

