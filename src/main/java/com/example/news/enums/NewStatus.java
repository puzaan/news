package com.example.news.enums;

public enum NewStatus {
    PUBLISHED, NOT_PUBLISHED
}
