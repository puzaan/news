package com.example.news.dto.newsdto;

import com.example.news.enums.NewStatus;
import com.example.news.enums.NewsType;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NewsDto {
    private String discription;
    @Enumerated(EnumType.STRING)
    private NewsType newsType;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate postedDate;
    @Enumerated(EnumType.STRING)
    private NewStatus newStatus;
    private Boolean highlight =false;
    private Integer newsCategoryId;
    private Integer attachmentId;
}
