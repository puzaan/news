package com.example.news.entity.news;

import com.example.news.entity.attachment.Attachment;
import com.example.news.entity.newsCategory.NewsCategory;
import com.example.news.enums.NewStatus;
import com.example.news.enums.NewsType;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "the_news")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class TheNews {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer id;
private String discription;
@Enumerated(EnumType.STRING)
private NewsType newsType;
@DateTimeFormat(pattern = "yyyy-MM-dd")
private LocalDate postedDate;
@Enumerated(EnumType.STRING)
private NewStatus newStatus;
private Boolean highlight =false;
    @ManyToOne()
    @JoinColumn(name = "new_category_id",
            foreignKey = @ForeignKey(name = "FK_New_CATEGORY"),
            referencedColumnName = "id")
    private NewsCategory newsCategory;
    @OneToOne
    @JoinColumn(name = "attachment_id", foreignKey = @ForeignKey(name = "FK_attachment_id"))
    private Attachment attachment;
    public TheNews(Integer theNewsId) {
        id = theNewsId;
    }

}
