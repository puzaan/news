package com.example.news.entity.newsCategory;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "news_category")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class NewsCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    public NewsCategory(Integer cateId) {
        id = cateId;
    }

}
