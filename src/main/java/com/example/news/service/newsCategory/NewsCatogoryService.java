package com.example.news.service.newsCategory;

import com.example.news.entity.newsCategory.NewsCategory;

import java.util.List;

public interface NewsCatogoryService {
    NewsCategory createNewCategory(NewsCategory newsCategory);
    List<NewsCategory> getAllNewCategoryList() ;

    NewsCategory getNewCategoryById(Integer id);

    public void deleteNewCategory(Integer id);

    NewsCategory updateNewCategoryDetail(NewsCategory newsCategory);
}
