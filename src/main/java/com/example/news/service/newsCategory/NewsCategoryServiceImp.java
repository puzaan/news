package com.example.news.service.newsCategory;

import com.example.news.entity.newsCategory.NewsCategory;
import com.example.news.globbal.CustomMessageSource;
import com.example.news.repo.newsCategory.NewsCategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsCategoryServiceImp implements NewsCatogoryService{

    private final NewsCategoryRepository newsCategoryRepository;

    private final CustomMessageSource customMessageSource;

    public NewsCategoryServiceImp(NewsCategoryRepository newsCategoryRepository, CustomMessageSource customMessageSource) {
        this.newsCategoryRepository = newsCategoryRepository;
        this.customMessageSource = customMessageSource;
    }

    @Override
    public NewsCategory createNewCategory(NewsCategory newsCategory) {
        return newsCategoryRepository.save(newsCategory);
    }

    @Override
    public List<NewsCategory> getAllNewCategoryList() {
        return newsCategoryRepository.findAll();
    }

    @Override
    public NewsCategory getNewCategoryById(Integer id) {
            return newsCategoryRepository.findById(id).orElseThrow(()->
                    new RuntimeException(
                            customMessageSource.get("error.not.found",
                                    customMessageSource.get("newCategory")
                            )
                    )
            );
    }

    @Override
    public void deleteNewCategory(Integer id) {
        getNewCategoryById(id);
        newsCategoryRepository.deleteById(id);
    }

    @Override
    public NewsCategory updateNewCategoryDetail(NewsCategory newsCategory) {
        getNewCategoryById(newsCategory.getId());
        return newsCategoryRepository.save(newsCategory);
    }
}
