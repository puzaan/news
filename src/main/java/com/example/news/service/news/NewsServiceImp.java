package com.example.news.service.news;

import com.example.news.dto.newsdto.NewsDto;
import com.example.news.entity.attachment.Attachment;
import com.example.news.entity.news.TheNews;
import com.example.news.entity.newsCategory.NewsCategory;
import com.example.news.enums.NewsType;
import com.example.news.globbal.CustomMessageSource;
import com.example.news.repo.news.NewsRepository;
import com.example.news.service.attachment.AttachmentService;
import com.example.news.service.newsCategory.NewsCatogoryService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NewsServiceImp implements NewService {

    private final NewsRepository newsRepository;
    private final CustomMessageSource customMessageSource;

    private final NewsCatogoryService newsCatogoryService;

    private final AttachmentService attachmentService;

    public NewsServiceImp(NewsRepository newsRepository, CustomMessageSource customMessageSource, NewsCatogoryService newsCatogoryService, AttachmentService attachmentService) {
        this.newsRepository = newsRepository;
        this.customMessageSource = customMessageSource;
        this.newsCatogoryService = newsCatogoryService;
        this.attachmentService = attachmentService;
    }

    @Override
    public TheNews createNew(NewsDto news) {
        Attachment attachment = attachmentService.singleAttachmentDetail(news.getAttachmentId());
        NewsCategory newsCategory  =newsCatogoryService.getNewCategoryById(news.getNewsCategoryId());
        TheNews news1  = TheNews.builder()
                .newStatus(news.getNewStatus())
                .newsCategory(newsCategory)
                .newsType(news.getNewsType())
                .attachment(attachment)
                .discription(news.getDiscription())
                .highlight(news.getHighlight())
                .newStatus(news.getNewStatus())
                .postedDate(news.getPostedDate())
                .build();
        return newsRepository.save(news1);
    }

    @Override
    public List<TheNews> getAllNewList() {
        return newsRepository.findAll();
    }

    @Override
    public TheNews getNewById(Integer id) {

        return newsRepository.findById(id).orElseThrow(()->
                new RuntimeException(
                        customMessageSource.get("error.not.found",
                                customMessageSource.get("news")
                        )
                )
        );    }

    @Override
    public void deleteNew(Integer id) {
        TheNews theNews = getNewById(id);
        newsRepository.deleteById(id);
        attachmentService.deleteAttachment(theNews.getAttachment().getAttachmentId());


    }

    @Override
    public TheNews updateNewDetail(TheNews news) {
        getNewById(news.getId());
        return newsRepository.save(news);
    }

    @Override
    public TheNews createNewsWithImage(MultipartFile file, Attachment attachment,NewsDto news) {
        Attachment attachment1 = attachmentService.saveAttachment( file,  attachment);
        NewsCategory newsCategory  =newsCatogoryService.getNewCategoryById(news.getNewsCategoryId());

        TheNews news1  = TheNews.builder()
                .newStatus(news.getNewStatus())
                .newsCategory(newsCategory)
                .newsType(news.getNewsType())
                .attachment(attachment1)
                .discription(news.getDiscription())
                .highlight(news.getHighlight())
                .newStatus(news.getNewStatus())
                .postedDate(news.getPostedDate())
                .build();
        return newsRepository.save(news1);
    }

    @Override
    public List<TheNews> findAllByNewsType(NewsType newsType) {
        return newsRepository.findAllByNewTypes(newsType);
    }

    @Override
    public List<TheNews> findAllHighlightTrueNews() {
        return newsRepository.findAllHighlightNewsTrue();
    }


    @Override
    public Map<String, List<TheNews>> getAllNewsByCategory() {
        List<NewsCategory> CategoryList = newsCatogoryService.getAllNewCategoryList();

        Map<String , List<TheNews>> newsCategoryList = new HashMap<>();
        for (NewsCategory  x : CategoryList){
            newsCategoryList.put(
                    x.getName(),
                    newsRepository.findAllNewByCategory(x.getId())
            );
        }
        return newsCategoryList;
    }
}
