package com.example.news.service.news;

import com.example.news.dto.newsdto.NewsDto;
import com.example.news.entity.attachment.Attachment;
import com.example.news.entity.news.TheNews;
import com.example.news.enums.NewsType;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface NewService {
    TheNews createNew(NewsDto news);
    List<TheNews> getAllNewList() ;

    TheNews getNewById(Integer id);

    public void deleteNew(Integer id);

    TheNews updateNewDetail(TheNews news);

    TheNews createNewsWithImage(MultipartFile file, Attachment attachment, NewsDto news);

    List<TheNews> findAllByNewsType(NewsType newsType);

    List<TheNews> findAllHighlightTrueNews();

    Map<String, List<TheNews>> getAllNewsByCategory();

}
