package com.example.news.service.attachment;

import com.example.news.entity.attachment.Attachment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public interface AttachmentService {

    Attachment saveAttachment(MultipartFile file, Attachment attachment);
    List<Attachment> getAllAttachmentDetails();

    Attachment singleAttachmentDetail(Integer attachmentId) ;

    void deleteAttachment(Integer attachmentId);

    byte[] getImageWithmediaType(Integer attachmentId) throws IOException;
}
