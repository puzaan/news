package com.example.news.service.attachment;


import com.example.news.entity.attachment.Attachment;
import com.example.news.globbal.CustomMessageSource;
import com.example.news.repo.attachment.AttachmentRepository;
import com.example.news.utils.FileUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.io.IOUtils;



import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class AttachmentServiceImp implements AttachmentService {

    private final AttachmentRepository attachmentRepository;

    private final static String rootPath = "attachments";


    private Path path = null;
    private Path imagePath = null;

    private final CustomMessageSource customMessageSource ;


    public AttachmentServiceImp(AttachmentRepository attachmentRepository, CustomMessageSource customMessageSource) {
        this.attachmentRepository = attachmentRepository;
        this.customMessageSource = customMessageSource;
    }


    @Override
    public Attachment saveAttachment(MultipartFile file, Attachment attachment)  {
        try {

            String timeStamps = String.valueOf(System.currentTimeMillis());
            imagePath = FileUtils.pathFinders( file,timeStamps);
            Files.createDirectories(imagePath);

            attachment.setImageFileHash(FileUtils.fileHash(file, timeStamps).toString());
            Files.copy(file.getInputStream(), imagePath.resolve(Objects.requireNonNull(file.getOriginalFilename())));

            return attachmentRepository.save(attachment);


        } catch (Exception e) {
            throw new RuntimeException(customMessageSource.get("error.file.type",
                    customMessageSource.get("attachment")));
        }
    }

    @Override
    public List<Attachment> getAllAttachmentDetails() {
        List<Attachment> attachmentDtoList = new ArrayList<>();
        List<Attachment> attachments = attachmentRepository.findAll();
        for(Attachment attachment : attachments) {
            Attachment attachmentDto = new Attachment();
            BeanUtils.copyProperties(attachment, attachmentDto);
            attachmentDtoList.add(attachmentDto);
        }
        return attachmentDtoList;
    }

    @Override
    public Attachment singleAttachmentDetail(Integer attachmentId)  {
        return attachmentRepository.findById(attachmentId).orElseThrow(() ->
                new RuntimeException(customMessageSource.get("error.not.found",
                        customMessageSource.get("attachment"))
                )
        );

    }


    @Override
    public void deleteAttachment(Integer attachmentId){
        Attachment attachment = singleAttachmentDetail(attachmentId);

        File hashMapPath = new File(rootPath.concat( "/" + attachment.getImageFileHash()));
        FileSystemUtils.deleteRecursively(hashMapPath);

        attachmentRepository.deleteById(attachmentId);

    }

    public byte[] getImageWithmediaType(Integer attachmentId) throws IOException {
        Attachment attachment = singleAttachmentDetail(attachmentId);
        String path = rootPath.concat("/" + attachment.getImageFileHash());
        Path imageUrl = getPath(path);
        if(imageUrl==null){
            new RuntimeException(customMessageSource.get("error.not.found",
                    customMessageSource.get("attachment"))
            );
        }
            return IOUtils.toByteArray(imageUrl.toUri());

    }
    private static Path getPath(String path) {
        File file = new File(path);
        String[] fileList = file.list();
        File imagePath = new File(path.concat("/" + fileList[0]));
        Path imageUrl = Paths.get(String.valueOf(imagePath));
        return imageUrl;
    }


}
